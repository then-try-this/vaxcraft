
;; -*- mode: scheme; -*-
;; Vaxcraft Copyright (C) 2021 FoAM Kernow
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.
;;
;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(load "scm/maths.jscm")
(load "scm/random.jscm")
(load "scm/nightjar.jscm")
(load "scm/local-storage.jscm")

(define player-name "???")
(define end-game-time 7)
(define installation-mode #t)
(define cheat-mode #f)
(define highlight-col "#2ceaa3")

;; delete old params if they are here
;(let ((v (local-load "vaxcraft-version")))
;  (when v
;		(msg "nuking local params")
;		(local-nuke)))

(define (param name)
  (string->number (local-get-param-fast "vaxcraft" name)))

(define (string-param name)
  (local-get-param-fast "vaxcraft" name))

(define (stage-reached)
  (string->number (local-get-param "vaxcraft" "player-stage-reached")))

(define (player-id)
  (string->number (local-get-param "vaxcraft" "player-id")))

(define (help-seen)
  (string->number (local-get-param "vaxcraft" "help-seen")))

(define (inc-help-seen!)
  (local-set-param "vaxcraft" "help-seen" (+ (help-seen) 1)))

(local-setup-params-fast
 "vaxcraft"
 (list
  (make-param "start-level" 1 "seconds")
  (make-param "world-level-time" 60 "seconds")
  (make-param "world-num-levels" 5 "levels")
  (make-param "world-max-viruses" 30 "viruses")
  (make-param "world-start-hosts" 10 "host cells")
  (make-param "world-infection-period" 0.5 "seconds")
  (make-param "world-vaccination-period" 3 "seconds")
  (make-param "world-mutate-power-mul" 0.1 "units")
  (make-param "world-mutate-power-drop" 0.1 "units")
  (make-param "host-receptor-count" 3 "receptors")
  (make-param "host-spawn-count" 2 "babies")
  (make-param "host-spawn-radius" 10 "pixels")
  (make-param "host-spawn-rate" 20 "seconds") 
  (make-param "host-max-population" 20 "cells")
  (make-param "host-max-age" 30 "seconds")
  (make-param "host-cell-size" 100 "pixels")
  (make-param "host-infected-death-time" 2 "seconds")
  (make-param "virus-receptor-count" 5 "receptors")
  (make-param "virus-size" 120 "pixels")
  (make-param "virus-max-age" 10 "seconds")
  (make-param "virus-cell-spawn-count" 5 "viruses")
  (make-param "petri-radius" 350 "pixels")
  (make-param "fluid-speed" 400 "x seconds")
  (make-param "fluid-scale" 0.002 "units")
  (make-param "fluid-strength" 12 "units")
  (make-param "model-mutation-rate" 20 "percent")
  (make-param "model-pop-threshold" 50 "percent")
  (make-param "model-population-size" 100 "individuals")
  (make-param "model-hosts-per-update" 3 "hosts")

  (make-param "model-full-immunity" 3 "shapes")
  (make-param "model-min-mut-duration" 5 "seconds")
  (make-param "model-max-mut-duration" 22.5 "seconds")
  (make-param "model-duration-power" 1 "power")
  (make-param "host-immune-min-strength" 3 "seconds")
  (make-param "host-immune-max-strength" 6 "seconds")
  (make-param "world-inject-prob" 5 "per second")
  (make-param "world-inject-time" 1.5 "seconds")
  
  ))

(define (new-player?)
  (not (local-exists? "vaxcraft-params")))

(define (init-player-params!)
  (local-setup-params
   "vaxcraft" 
   (list
	(make-param "player-stage-reached" 0 "stage")
	(make-param "player-id" -1 "number")
	(make-param "help-seen" -1 "number"))))

(define params-version 25)

(define (init-params!)
  (let ((v (local-load "vaxcraft-version")))
	(when (or (not v) (< v params-version))
		  (msg "resetting params")
		  (local-nuke)
		  (init-player-params!)
		  (local-save "vaxcraft-version" params-version)
		  )))


(load "scm/ordered-list.jscm")
(load "scm/particles.jscm")
(load "scm/animation.jscm")
(load "scm/entity.jscm")
(load "scm/entity-list.jscm")
(load "scm/entity-renderer.jscm")

(load "scm/scores.jscm")
(load "scm/help-bubbles.jscm")
(load "scm/receptors.jscm")
(load "scm/model.jscm")
(load "scm/virus.jscm")
(load "scm/host.jscm")
(load "scm/world.jscm")
(load "scm/levels.jscm")
(load "scm/vaccination.jscm")
(load "scm/game-help.jscm")
(load "scm/demo.jscm")

(define (button-sound)
  (play-sound "button.wav"))

(define default-button-x (- (/ screen-width 2) 200))
(define default-button-y (+ (/ screen-height 2) 100))
(define default-button-width 300)
(define default-button-height 80)

(define button-gap 250)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define (register-new-game c stage)
  (register-player! 
   (lambda ()
	 (register-game c (player-id) stage 1 2)))
  c)

(define (register-game c player-id stage level lives)
  (server-call-mutate
   "game"
   (list (list "player_id" player-id))
   (lambda (c game-id)
     (nightjar-game-screen c game-id stage level lives 0)))
  c)

(define (score-list game-id stage level)
  (list
   (list "game_id" game-id)
   (list "stage" stage)
   (list "level" level)
   (list "survived" (scores-survived))
   (list "duration" (scores-duration))
   (dbg (list "mutations" (scores-mutations)))
   (list "hosts_spawned" (scores-hosts-spawned))
   (list "viruses_spawned" (scores-viruses-spawned))
   (list "infections" (scores-infections))
   (list "max_hosts" (scores-max-hosts))
   (list "max_viruses" (scores-max-viruses))
   (list "final_hosts" (scores-final-hosts))
   (list "final_viruses" (scores-final-viruses))))

(define (save-scores! game-id stage level)
  (server-call "score" (score-list game-id stage level)))

(define (save-scores-and-end c game-id)
  (let ((w (game-data c)))
	(server-call-mutate
	 "score"
	 (score-list
	  game-id
	  (world-stage w)
	  (world-level w))
	 (lambda (c score-pos)
	  (nightjar-end c score-pos (world-stage w) (world-level w))))
	c))

(define (nightjar-intro c)
  (cond
   ((new-player?)
	(game-modify-render
	 (lambda (ctx)
	   (set! ctx.fillStyle text-col)		
	   (set! ctx.font "normal 120pt Dosis")
	   (wrap-text ctx "VAXCRAFT" 0 200 1500 100)		
	   (set! ctx.font "normal 30pt Dosis")
	   (wrap-text ctx "Are you happy for us to store your player info (scores and level reached so far) on your browser and on our server for the leaderboard? This does not include any personally identifiable information." 0 340 1200 60)
	   (set! ctx.font "normal 30pt Dosis")
	   )
	 
	 (game-modify-buttons
	  (list
	   
	   (image-button
		"YES" 
		(- default-button-x 300)
		default-button-y
		"jumpy"
		(find-image "buttons/button-2.png")
		(lambda (c)
		  (init-player-params!)
		  (button-sound)
		  (nightjar-main c)))
	   
	   (image-button
		"NO" 
		(+ default-button-x 300)
		default-button-y
		"jumpy"
		(find-image "buttons/button-3.png")
		(lambda (c)
		  (button-sound)
		  (set! window.location "https://benashbyevo.wordpress.com/")))
	   
	   )
	  c)))
   (else
	(nightjar-main c))))

(define (register-player! next)
  (init-params!)
  (cond
   ((eq? (player-id) -1)
	(server-call-mutate
	 "player"
	 '() 
	 (lambda (c id)	 
	   ;; save it...
	   (local-set-param "vaxcraft" "player-id" id)
	   (next))))
   (else
	(if (eq? player-name "???")
		(server-call-mutate
		 "get-player-name"
		 (list (list "player_id" (player-id)))
		 (lambda (c name) 
		   ;; save it...
		   (set! player-name (JSON.parse name))
		   (next)))
		(next)))
	))

(define (nightjar-main c)
  (game-modify-mouse-hook
   (lambda (state c) c)
   (game-modify-timeout
	(lambda ()
	  (when (not ditto.load_resource_static)	
			(set! window.location "/")))
	(game-modify-data
	 (lambda (d)
	   (make-new-demo))
	 (game-modify-update
	  (lambda (t c)
		(game-modify-data
		 (lambda (d)
		   (demo-update (game-data c) (/ t 1000) (game-delta c)))
		 ;; hand wavy detection
		 (if (and installation-mode
			  (not (eq? (get-sensor-state-debounced) "none")))
		     (nightjar-about c)
			 c)))
	  (game-modify-render
	   (lambda (ctx)
		 (demo-render (game-data c) ctx)
		 (set! ctx.fillStyle text-col)		
		 (set! ctx.font "normal 120pt Dosis")
		 (wrap-text ctx "VAXCRAFT" 0 200 1500 100)		
		 (set! ctx.font "normal 30pt Dosis")		 
		 (wrap-text ctx "These creatures are at risk of infection, but you can save them through vaccination." 0 320 1000 40)
		 (when installation-mode
           (render-installation-message
            ctx "Hover your hand over one of the wooden boxes to start"
            420 (/ (game-time c) 1000)))
		 (ctx.drawImage (find-image "logos.png") 380 620)
		 )
	   
	   (game-modify-buttons
		(if installation-mode
			
			(list
			 (hidden-button
			  ""
			  (+ default-button-x 600)
			  (+ default-button-y 90)
			  500 300
			  #f
			  (lambda (c)
				(button-sound)
				(nightjar-about c)))

			 (hidden-button
			  "" 0 0 100 100
			  #f
			  (lambda (c)
				(button-sound)
				(nightjar-secrets c)))
			 )
			
			(list

			 (image-button
			  "" 1390 550 #f
              (find-image "buttons/fullscreen.png")
			  (lambda (c)
				(button-sound)
                (let ((el (document.getElementById "game-holder")))
                  (el.requestFullscreen))
                c))
             
			 (image-button
			  "PLAY!"
			  default-button-x
			  (- default-button-y 80)
			  "jumpy"
			  (find-image "buttons/button-1.png")
			  (lambda (c)
				(button-sound)
				(register-new-game c 0)))
			 
			 (image-button
			  "HOW TO PLAY"
			  (- default-button-x 400)
			  (- default-button-y 80)
			  "jumpy"
			  (find-image "buttons/button-2.png")
			  (lambda (c)
				(button-sound)
				(nightjar-about c)))
			 
			 (image-button
			  "LEADERBOARD"
			  (+ default-button-x 400)
			  (- default-button-y 80)
			  "jumpy"
			  (find-image "buttons/button-3.png")
			  (lambda (c)
				(button-sound)
				(server-call-mutate
				 "hiscores"
				 (list)
				 (lambda (c scores)
				   (nightjar-hiscores c (JSON.parse scores))))
				c))
			 
			 ))
			c)))))))

(define (nightjar-about c)
  (game-modify-render
   (lambda (ctx)
	 ;;(demo-render (game-data c) ctx)
	 (if installation-mode
		 (ctx.drawImage (find-image "about.png") 0 -25)
		 (ctx.drawImage (find-image "about.png") 0 -25))

     (when installation-mode
       (render-installation-message
        ctx ""
        550 (/ (game-time c) 1000)))

	 (set! ctx.font "normal 30pt Dosis")
	 )
   (game-modify-buttons
	(if installation-mode
		(list
		 (hidden-button
		  ""
		  (+ default-button-x 600)
 		  (+ default-button-y 90)
		  500 300
		  #f
		  (lambda (c)
		    (button-sound)
		    (register-new-game c 0))))
		(list
		 (image-button
		  "BACK"
		  (- default-button-x 400)
		  (+ default-button-y 90)
		  "jumpy"
		  (find-image "buttons/button-3.png")
		  (lambda (c)
			(button-sound)
			(nightjar-main c)))

		 (image-button
		  "MORE INFO"
		  (+ default-button-x 400)
		  (+ default-button-y 90)
		  "jumpy"
		  (find-image "buttons/button-1.png")
		  (lambda (c)
			(button-sound)
			(nightjar-science c)))
		 ))
	(if installation-mode
		(game-modify-update
		 (lambda (t c)
		   (if (not (eq? (get-sensor-state-debounced) "none"))
			   (register-new-game c 0)
			   c))
		 c)
		c))))

(define (nightjar-science c)
  (game-modify-render
   (lambda (ctx)
	 (demo-render (game-data c) ctx)
	 (ctx.drawImage (find-image "ben.png") 0 -25)
	 (set! ctx.font "normal 30pt Dosis")
	 )

   (game-modify-buttons
	(list
	 (image-button
	  "BACK"
	  (- default-button-x 300)
	  (+ default-button-y 90)
	  "jumpy"
	  (find-image "buttons/button-1.png")
	  (lambda (c)
		(button-sound)
		(nightjar-about c)))

	 (image-button
	  "BEN'S WEBSITE"
	  (+ default-button-x 300)
	  (+ default-button-y 90)
	  "jumpy"
	  (find-image "buttons/button-3.png")
	  (lambda (c)
		(button-sound)
		(set! window.location "https://benashbyevo.wordpress.com/")
		c))

	 (hidden-button
	  "" 1260 210 50 50 #f
	  (lambda (c)
		(button-sound)
		(set! cheat-mode #t)
		c))	 
	 )
	c)))

(define (nightjar-game-screen c game-id stage level lives vaccinations)
  (game-modify-data
   (lambda (d)
	 (build-world stage level game-id (/ (game-time c) 1000) lives vaccinations))
   (game-modify-mouse-hook
	(lambda (state c)
	  (game-modify-data
	   (lambda (d)
		 (world-update-mouse (game-data c) state (game-mx c) (game-my c) (/ (game-time c) 1000)))
	   c))   
	(game-modify-render
	 (lambda (ctx)
	   (world-render (game-data c) ctx (/ (game-time c) 1000))
	   (set! ctx.font "normal 20pt Dosis")
	   )
 	 (game-modify-update
	  (lambda (t c)
	    (let ((w (game-data c)))
          (cond
		   ((eq? (world-state w) "state-exit")
			;; exit game
		    (save-scores-and-end c game-id)) 		   
		   ((eq? (world-state w) "state-restart-level")
			;; try again
		    (nightjar-game-screen c game-id (world-stage w) (world-level w) (- (world-lives w) 1) (scores-mutations)))
		   ((eq? (world-state w) "state-new-level")
			;; new level
		    (nightjar-game-screen c game-id (world-stage w) (+ (world-level w) 1) (world-lives w) (scores-mutations)))
		   
		   ;; don't do anything...
		   ((eq? (world-state w) "state-waiting-for-restart") c)		   
		   (else			   
			(game-modify-data
			 (lambda (d)
			   ;; normal update
			   (world-update
			    ;; do the mouse update when using sensors
			    (if installation-mode (world-update-mouse d "" 0 0 (/ (game-time c) 1000)) d)
			    (/ t 1000) (game-delta c))
			   )
			 c)))))
	  (game-modify-buttons
	   (append
		(if installation-mode
			(list
             (rect-button
			  "QUIT" 30 (+ default-button-y 160) 80 50 #f
			  (lambda (c)
				(button-sound)
				;; (let ((w (game-data c)))
				;;   (update-scores-duration! (world-level-time w))
				;;   (update-scores-final-hosts! (length (world-hosts w)))
				;;   (update-scores-final-viruses! (length (world-viruses w)))
				;;  (save-scores-and-end c game-id))
				(nightjar-main c)
				)))
			(list
			 (rect-button
			  "QUIT" 30 (+ default-button-y 160) 80 50 #f
			  (lambda (c)
				(button-sound)
				;; (let ((w (game-data c)))
				;;   (update-scores-duration! (world-level-time w))
				;;   (update-scores-final-hosts! (length (world-hosts w)))
				;;   (update-scores-final-viruses! (length (world-viruses w)))
				;;  (save-scores-and-end c game-id))
				(nightjar-main c)
				))

			 (rect-button
			  "HELP" 120 (+ default-button-y 160) 80 50 #f
			  (lambda (c)
				(button-sound)
				(game-modify-data
				 (lambda (d)
				   (set! help-stack (build-help
									 (world-game-type d)
									 (/ (game-time c) 1000)))
				   (world-modify-state d "state-help-paused"))
				 c)))
			 
			(rect-button
			 "PAUSE" 210 (+ default-button-y 160) 80 50 #f
			  (lambda (c)
				(button-sound)
                (if (null? help-stack)
                    (game-modify-data
                     (lambda (d)
                       (if (eq? (world-state d) "state-running")
                           (world-modify-state d "state-paused")
                           (world-modify-state d "state-running")))
                     c)
                    c))))

			)) c))))))


(load "scm/slider.jscm")
(define installation-immunity-multiplier 1)
(define installation-mutation-duration 10) 
 
(define (nightjar-secrets c)
  (define immunity-slider
    (make-slider 50 280 300 (* 300 (/ (- installation-immunity-multiplier 1) 3))
                 (lambda (v)
                   (let ((v (+ 1 (* (/ v 300) 3))))
                     (set! installation-immunity-multiplier v)                      
                     0))))
  (define mutation-slider
    (make-slider 50 480 300 (* 300 (/ installation-mutation-duration 60))
                 (lambda (v)
                   (let ((v (+ 1 (* (/ v 300) 59))))
                     (set! installation-mutation-duration v)                      
                     0))))
    
  (game-modify-render
   (lambda (ctx)     
     (set! ctx.font "normal 50pt Dosis")
     (wrap-text ctx "SECRET PAGE" 0 70 1300 100)
     (set! ctx.font "normal 30pt Dosis")
     (set! immunity-slider (slider-update immunity-slider))
     (slider-render! immunity-slider)
     (wrap-text-left ctx (+ "Immunity time mult: "
                            (/ (trunc (* installation-immunity-multiplier 100)) 100))
                     10 220 1300 100)
     (set! mutation-slider (slider-update mutation-slider))
     (slider-render! mutation-slider)
     (wrap-text-left ctx (+ "Mutate every (secs): "
                            (trunc installation-mutation-duration))
                     10 420 1300 100)
     
     )
   (game-modify-buttons
    (list
     (image-button
      "HALT PI"
      default-button-x
      (- default-button-y 400)
      "jumpy"
      (find-image "buttons/button-1.png")
      (lambda (c)
	(button-sound)
	(halt-pi)
	c))
     (image-button
      "TEST SHAPES"
      default-button-x
      (- default-button-y 200)
      "jumpy"
      (find-image "buttons/button-1.png")
      (lambda (c)
	(button-sound)
	(test-shapes)
	c))
     (image-button
      "BACK"
      default-button-x
      default-button-y
      "jumpy"
      (find-image "buttons/button-2.png")
      (lambda (c)
	(button-sound)
	(nightjar-main c)
	))) c)))

(define scores-anim-t 0)
(define scores-anim-nests 0)
(define scores-anim-activity 0)
(define scores-anim-timing 0.10) 

(define (to-fixed v n)
  (v.toFixed n))

(define (pluralise v text)
  (string-append v (if (eq? v 1) text (string-append text "s"))))

(define end-screen-time 8)
(define end-screen-timer 0)

(define (nightjar-end c score-pos stage level)
  (set! end-screen-timer 0)
  (cond
   ((and (eq? player-name "???")
		 (not installation-mode))
	(nightjar-enter-name c score-pos stage level))
   (else
	(set! scores-anim-pos 0)
	(game-modify-mouse-hook
	 (lambda (state c) c)
	 (game-modify-render
	  (lambda (ctx)
		(set! ctx.fillStyle text-col)      
		
		(let ((n (game-data c)))		
		  (set! ctx.fillStyle text-col)      		  
		  ;; (wrap-text
		  ;;  ctx 
		  ;;  (string-append "YOU SURVIVED " (to-fixed (scores-duration) 2)
		  ;; 				  " SECONDS INTO LEVEL " level
		  ;; 				  " OF THE " (cond
		  ;; 							  ((eq? stage 0) "SHAPE STAGE")
		  ;; 							  ((eq? stage 1) "MUTATE STAGE")
		  ;; 							  ((eq? stage 2) "MULTIVARIANT STAGE")
		  ;; 							  (else "HIDDEN STAGE")))
		  ;;  0 100 1300 100)

		  ;;(set! ctx.font "normal 30pt Dosis")

		  (set! ctx.font "normal 75pt Dosis")

		  (cond
		   (installation-mode
			(wrap-text
			 ctx
			 (string-append "YOU SURVIVED " (to-fixed (scores-duration) 2)
							" SECONDS INTO LEVEL " level)
			 0 150 1300 130)

			(set! ctx.font "normal 30pt Dosis")

			(wrap-text
			 ctx 
			 (string-append "Well done, this gives you a high score position of: " score-pos)
			 0 400 1300 100)
			
			)
		   (else
			(wrap-text
			 ctx 
			 (string-append "Well done, your high score position is " score-pos)
			 0 150 1300 130)
			
			(set! ctx.font "normal 30pt Dosis")
			
			(wrap-text
			 ctx 
			 (cond
			  ((eq? score-pos 1)
			   "YOU ARE OFFICIALLY TOP VIROLOGIST!")
			  ((<= score-pos 10)
			   "CONGRATULATIONS: YOU MADE THE TOP TEN!")
			  (else
			   "Can you make the top ten?"))
			 0 400 1300 100)))
		  
		  		  
		  ;; (wrap-text
		  ;;  ctx 
		  ;;  (string-append "You spawned " (pluralise (scores-hosts-spawned) " host"))
		  ;;  0 350 1300 100)
		  ;; (wrap-text
		  ;;  ctx 
		  ;;  (string-append "Had " (pluralise (scores-infections) " infection"))
		  ;;  0 400 1300 100)
		  ;; (wrap-text
		  ;;  ctx 
		  ;;  (string-append "With a max population of " (pluralise (scores-max-hosts) " host"))
		  ;;  0 450 1300 100)
		  

		  (set! ctx.font "bold 30pt Dosis")	
		  (set! ctx.font "normal 30pt Dosis")
		  )
		)
	  (game-modify-update
	   (lambda (t c)
		 (cond
		  (installation-mode
		   (set! end-screen-timer (+ end-screen-timer (game-delta c)))
		   (if (> end-screen-timer end-screen-time)
			   (nightjar-main c)
			   c))
		  (else c)))
	   (game-modify-buttons
		(if installation-mode
			'()
			(list
			 (image-button
			  "PLAY AGAIN"
			  (- default-button-x 400)
			  default-button-y
			  "jumpy"
			  (find-image "buttons/button-2.png")		  
			  (lambda (c)
				(button-sound)
				(game-modify-data
				 (lambda (d)
				   (make-new-demo))
				 (register-new-game c 0))
				))

			 (image-button
			  "BACK"
			  (+ default-button-x 400)
			  default-button-y
			  "jumpy"
			  (find-image "buttons/button-3.png")		  
			  (lambda (c)
				(button-sound)
				(nightjar-intro game)))
			 
			 (image-button
			  "LEADERBOARD"
			  default-button-x
			  default-button-y
			  "jumpy"
			  (find-image "buttons/button-1.png")		  
			  (lambda (c)
				(button-sound)
				(server-call-mutate
				 "hiscores"
				 (list)
				 (lambda (c scores)	      
				   ;; restart the demo!
				   (nightjar-hiscores
					(game-modify-update
					 (lambda (t c)
					   ;; update loop
					   (game-modify-data
						(lambda (d)
						  (demo-update (game-data c) (/ t 1000) (game-delta c)))
						c))
					 ;; init the demo
					 (game-modify-data
					  (lambda (d)
						(make-new-demo))
					  c))
					(JSON.parse scores))))
				c))))
		
		c)))))))

(define col1 -350)
(define col2 -150)
(define col3 90)
(define col4 300)

(define (nightjar-hiscores c scores)
  (game-modify-render
   (lambda (ctx)
	 (demo-render (game-data c) ctx)
	 (set! ctx.fillStyle text-col)      
	 (set! ctx.font "normal 40pt Dosis")
	 (wrap-text ctx "TOP TEN PLAYERS THIS WEEK" -20 60 1300 100)
	 (set! ctx.font "normal 25pt Dosis")
	 
     (wrap-text ctx "NAME" col1 170 1000 1)
     (wrap-text ctx "VACCINATIONS" col2 170 1000 1)
     (wrap-text ctx "TIME SURVIVED" col3 170 1000 1)
     (wrap-text ctx "SCORE" col4 170 1000 1)

	 (set! ctx.font "normal 20pt Dosis")
     (wrap-text ctx "The highest scoring players survived longest with the minimum number of vaccinations."
                -20 100 1000 30)

	 (set! ctx.font "normal 25pt Dosis")
	 
	 (define lh 38)
	 (set! ctx.fillStyle text-col)
	 
     (when scores
	   (index-for-each
	    (lambda (i e)
	      (cond
	       ((zero? i)
			(set! ctx.font "bold 25pt Dosis")
			(wrap-text ctx (list-ref e 0) col1 (+ 220 (* lh i)) 1000 1)
			(wrap-text ctx (number->string (list-ref e 1)) col2 (+ 220 (* lh i)) 1000 1)
			(wrap-text ctx (string-append (floor 
                                           (+ (* (- (list-ref e 2) 1) 60) (list-ref e 3))
                                           ) " secs") col3 (+ 220 (* lh i)) 1000 1)
			(wrap-text ctx (number->string (floor (list-ref e 4))) col4 (+ 220 (* lh i)) 1000 1))
	       (else
			(set! ctx.font "normal 25pt Dosis")
			(wrap-text ctx (list-ref e 0) col1 (+ 220 (* lh i)) 1000 1)
			(wrap-text ctx (number->string (list-ref e 1)) col2 (+ 220 (* lh i)) 1000 1)
			(wrap-text ctx (string-append (floor 
                                           (+ (* (- (list-ref e 2) 1) 60) (list-ref e 3))
                                           ) " secs") col3 (+ 220 (* lh i)) 1000 1)			
			(wrap-text ctx (number->string (floor (list-ref e 4))) col4 (+ 220 (* lh i)) 1000 1))))
	    scores))	 
	 )
   (game-modify-buttons
	(list
	 (image-button
	  "BACK"
	  default-button-x
	  (+ default-button-y 85)
	  "jumpy"
	  (find-image "buttons/button-2.png")		  
	  (lambda (c)
		(button-sound)
		(nightjar-intro game))))
	c)))

(define (type-into str ch)
  (car
   (foldl
	(lambda (c r)
	  (if (and (not (cadr r)) (eq? c "?"))
		  (list (string-append (car r) ch) #t)
		  (list (string-append (car r) c) (cadr r))))
	(list "" #f)
	(str.split ""))))

(define (type-into-delete str)
  (car (foldl
		(lambda (c r)
		  ;;(console.log (list c r))
		  (if (and (not (cadr r)) (not (eq? c "?")))
			  (list (string-append "?" (car r)) #t)
			  (list (string-append c (car r)) (cadr r))))
		(list "" #f)
		(reverse (str.split "")))))

(define (type-username ch c)
  (set! player-name (type-into player-name ch)))

(define (type-delete c)
  (set! player-name (type-into-delete player-name)))


(define (qwertypos-x i)  
  (cond ((< i 10) i)
		((and (> i 9) (< i 19)) (- i 10))
		(else (- i 19))))

(define (qwertypos-y i)  
  (cond ((< i 10) 0)
		((and (> i 9) (< i 19)) 1)
		(else 2)))

(define (nightjar-enter-name c score-pos stage level)
  (game-modify-mouse-hook
   (lambda (state c) c)
   (game-modify-update
	(lambda (t c) c)
	(game-modify-render
	 (lambda (ctx)
	   (set! ctx.font "normal 30pt Dosis")
	   (wrap-text ctx "Enter your name for the leaderboard" 0 70 1000 70)
	   (set! ctx.font "normal 50pt Dosis")
	   (wrap-text ctx player-name 0 180 1000 70)
	   (set! ctx.font "normal 30pt Dosis"))

	 (game-modify-buttons
	  (append
	   (index-map
		(lambda (i ch)
		  (let ((x (+ (cond ((eq? (qwertypos-y i) 0) 250)
							((eq? (qwertypos-y i) 1) 300)
							(else 380))
					  (* (qwertypos-x i) 120)))
				(y (+ 190 (* (qwertypos-y i) 130)))
				(img (find-image "sprites/hex-b-1.png")))
			(image-button ch x y #f
						  img
						  (lambda (c)
							(button-sound)
							(type-username ch c)
							c))))
		(list "Q" "W" "E" "R" "T" "Y" "U" "I" "O" "P"
			  "A" "S" "D" "F" "G" "H" "J" "K" "L"
			  "Z" "X" "C" "V" "B" "N" "M"))

	   (list
		(image-button
		 "DELETE"
		 (- default-button-x 200)
		 (+ default-button-y 80)
		 "jumpy"
		 (find-image "buttons/button-2.png")		  					  
		 (lambda (c)
		   (button-sound)		      
		   (type-delete c)
		   c))

		(image-button
		 "DONE"
		 (+ default-button-x 200)
		 (+ default-button-y 80)
		 "jumpy"
		 (find-image "buttons/button-3.png")		  					  
		 (lambda (c)
		   (button-sound)
		   (server-call-mutate
			"player-name"
			(list
			 (list "player_id" (player-id))
			 (list "player_name" player-name))
			(lambda (c data)
			  (nightjar-end c score-pos stage level)))
		   c))
		))
	  c)))))


(set! ctx.font "normal 75pt Dosis")
(set! ctx.fillStyle "#fff")

(load-sounds!
 (list "button.wav"
	   "death.wav"
	   "dice-click.wav"
	   "game-over.wav"
	   "level-complete.wav"
	   "life-lost.wav"
	   "mutate.wav"
	   "start.wav"
	   "shape-click.wav"
	   "spawn.wav"
	   "stage-complete.wav"
       "inject.wav"
       "immune-1.wav"
       "immune-2.wav"
       "immune-3.wav"
       "infect.wav"
	   ))

(load-images!
 (append 
  (anim->filenames virus-animation (list ""))
  (anim->filenames virus-variant-animation (list ""))
  (anim->filenames host-animation (list "_cir" "_gui" "_squ" "_tri"))
  (virus-receptor-filenames)
  (vaccination-filenames)
  (list
   "sprites/host/infected.png"
   "sprites/host/immune.png"
   "sprites/host/immune-1.png"
   "sprites/host/immune-2.png"
   "sprites/host/immune-3.png"
   "sprites/host/immune-4.png"
   "sprites/host/immune-5.png"
   "sprites/virus/question.png"
   "sprites/virus/particle.png"
   "sprites/immune/cir_green.png"
   "sprites/immune/gui_green.png"
   "sprites/immune/squ_green.png"
   "sprites/immune/tri_green.png"
   "sprites/immune/cir_grey.png"
   "sprites/immune/gui_grey.png"
   "sprites/immune/squ_grey.png"
   "sprites/immune/tri_grey.png"
   "sprites/hex-b-1.png"
   "logos.png"
   "help-bubble.png"
   "help-bubble-n.png"
   "help-bubble-r.png"
   "help-bubble-lt.png"
   "buttons/button-1.png"
   "buttons/button-2.png"
   "buttons/button-3.png"
   "buttons/fullscreen.png"
   "about.png"
   "about-installation.png"
   "ben.png"
   "hand.png"
   "boxes.png"
   ))

 (lambda ()
   (js "$('#html').css('background-color','black')")
   (start-game canvas ctx)))

(document.addEventListener
 "visibilitychange"
 (lambda ()
   (when document.hidden 
		 (set! window.location "/"))) #f)
